(function() {
	$.ajax({
		url: "/api/projects",
		type: "GET",
		success: function(data) {
			var data = JSON.parse(data);
			var key  = createKeyMenu(data);
			createCampaignMenus(key);
		},
		error: function(xhr) {
			if (xhr.status === 401) {
				$(".credentials a").toggleClass("hidden");
			}
		}
	});

	function createKeyMenu(data) {
		var defaultKey;
		$("#key").replaceWith($('<select id="key" name="key" style="color=#EEEEEE">'));
		$("input[name='params[campaign]']").replaceWith("<select name='params[campaign]'>");

		$.each(data, function(i, val) {
			if (val.default === true) {
				defaultKey = val.api_key
				updateKeyFields(defaultKey);
				$("#key").append('<option value="' + val.api_key + '"selected>' + val.id + '</option>');
			} else {
				$("#key").append('<option value="' + val.api_key + '">' + val.id + '</option>');
			}
		});	

		$("#key").change(function() {
			var key = $("#key option:selected").val();
			updateKeyFields(key);
			createCampaignMenus(key);
		});

		return defaultKey;
	}

	function createCampaignMenus(key) {
		var campaigns = ['<option></option>'];

		$("select[name='params[campaign]']").empty();

		$.ajax({
			url: "http://api.awe.sm/stats/range/options.json?v=3&key=" + key + "&group_by=campaign",
			type: "GET",
			success: function(data) {
				$.each(data.groups, function(i, val) {
					campaigns.push('<option>' + val.campaign + '</option>');
				});

				if (campaigns.length > 0) {
					var options = campaigns.join("");	
					$("select[name='params[campaign]']").append(options);
				}
			},
			error: function(xhr) {
				if (xhr.status === 401) {
					console.log("could not get campaigns")
				}
			}
		});
	}

	function updateKeyFields(key) {
		$("input[name='params[key]']").val(key);
	}
})();